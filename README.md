# README #

Group project for CS494P: Internetworking Protocols

This is an implementation of a simple file transfer application. 
We will implement our own protocol similar to a simplified version of RCP.
Transport Layer: UDP Sockets

Protocol:
1. Must establish a handshake between client and server. Client sends connection request packet,
   then the server sends a connection acknowledgement packet to client.
2. After establishing connection, client can request file by sending sending a file request packet.
   If file does not exist then server will notify client and terminate connection.
3. Packets may get lost so a timer will be used. Lets say if after sending a packet, if the timer runs
   out before we get an ACK we will resend the packet. In any case of duplicates we will drop the duplicate packet.
4. After transfer is done, server must notify client using a close packet then close conneciton.
   Once the client receives the packet they can also terminate connection and exit.
   
   
Note added 3-5: *********
Server

Sock - receiving from client
Socket2- sending to client

ackThread- uses sock and loops until file is done (pthread)


Client - still needs second thread

sock - sendTo function (send to server)
recvSock- recvFrom function (recv from server)

The way the client has to make the connection work properly is to send two initial connect
requests in the CONNECT case that way both sockets on the server side can associate the 
proper socket values with the client sockets. 






The way I have it set up - there will be a static array that will show the acked packets
so 0 means it hasn't been acked, 1 means it has, and for the client we may possibly
need to indicate that it has already been written to the file so maybe another ack value (possibly the number 2).
The thread for the client is tricky. Thats where different values in an acked array become important.
That way that the sender in the client can loop through an array to send ack packets for
packets that have been read into the file. 

The array reset that I have set up in both seems to be working but I think there is
a border case around the array that is causing the array reset and also the seq number
reset to fail. Right now it will send properly but if a packet is lost (or possibly received out of order)
it will fail. I set a clock_t object to be intialized when the oldest ack has been sent.
Raoul said that maybe we could do a timer for each packet (Seems like alot of work so I avoided that).

The client can handle packets being received out of order if we use fseek and fwrite to write
at particular locations in the file (which can be determined by the buffer size * sequence number).


Todo:

1- possibly need to add retransmissions during the regular states but that isn't tough
2- main problem: figuring out what to do when a packet expires
3- Adjust the window to increase after acks are received (easy to do, can just increase the window 
up to the static array of 50 that I made in the server- seems like that should be enough).
4- adjust window after missing ack 
5- *** create thread for client 

