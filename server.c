/* 
  Ian Percy, Edwin Ortega, and Joseph Sands
  CS494P Winter 2018, Programming Project P2
*/

#include <sys/types.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <netdb.h>
#include <stdio.h>
#include <time.h>
#include <pthread.h>

/*
  Server class
  - Will connect with client and send file if it exists
  - Two sockets - one for sending one for receiving
  - Two threads- one for sending and one for looping on receiving
*/

//ENUM defined for the states - Only difference between Server and Client is that Server listens and sends 
typedef enum{
	LISTEN,
	REQUEST,
	SENDFILE,
	DISCONNECT,
	CLOSED
} states;


void error(const char *msg)	// error message function which exits at the end.
{
    perror(msg);
    exit(0);
}

//Adding variables globally so they can be accessed both threads
    int i,seq,lowestAck,windowSize,fileNotDone, fileEndReached;  
    char packID;
    int diff,idx,temp,tempSize,recseqNum=0;
    int sz=0;
    int port,port2;
    socklen_t fromlen;
    struct sockaddr_in server,server2;
    struct sockaddr_in from,from2;
    char buf[sizeof(char) + sizeof(int)*2+sizeof(char)*1024]; // this is the "packet" AKA message being sent. 
    FILE *fp = NULL;
    
    size_t bytesRead = 0;
    char fileAddr[35];
    states check;  // variable to hold state
  
    int acked[50]; //array to hold acks
    int lastAck,threadValue;
    double msec;
    int sock, socket2, length, n,off, file_size;
    clock_t before,difference; 
    pthread_t ackThread; // thread to be created for acking locks
    pthread_mutex_t lock; // thread lock
   
// Function that will be run from the thread created during the file send process  
void * ackFunction(void * arg){
    while(!fileNotDone){ // loop until end of file reached
        // bzero(buf, sizeof(buf));    // temp_buf will now store received packet - dont clear buffer especially while not holding lock
        printf("recv thread");
        if(recvfrom(sock,buf,sizeof(buf),0,(struct sockaddr *) &from, &fromlen)>0){
            // if packet received 		   
            pthread_mutex_lock(&lock);		            // grab lock
            memcpy(&packID, buf, sizeof(packID));           // Storing PackID to check if it's 'A' for ACK
            off = sizeof(packID);
            memcpy(&recseqNum, buf+off, sizeof(recseqNum)); // Storing recseqNum to check if equal
            temp=ntohl(recseqNum);
            printf("seq %d \n",temp); 
            acked[temp]=1;					   
            if(temp==lowestAck){                            // adjust the window if the ack is the lowest one we are looking for
                for(i=0;i<windowSize;i++){
                    if(acked[i]==0){
                        break;
                    }	
                }
		if(i==windowSize){
                    i=0;                            // if entire window is acked - then move window up by the window size
                    lowestAck=lowestAck+windowSize;
                    seq=lowestAck;
                }
                else{
                    lowestAck=lowestAck+i+1;       // move ack up to closest spot in Acked[] where value ==0
                }
                diff=windowSize-i;
                for(i=0;i<windowSize;i++){         // copy values to fit into array properly
                    if(i>(windowSize-diff)){
                        acked[i]=0;
                    }
                    else{
                        acked[i]=acked[diff];
                    }	
                }
            } 			
            if(fileEndReached && lastAck ==temp){
                fileNotDone=1; 
                pthread_exit(0); // close thread once end reached
            }
            if(packID != 'A' || temp != seq){
                // an error has occured, handle this
                // retransmit packet or just terminate the connection 
            }
            else{
                // ACK with correct sequence number received, zero out buffers and loop again
                bzero(buf, sizeof(buf));
                lowestAck=seq;					   
            }  
            pthread_mutex_unlock(&lock);	 // release lock	  
        }
    }
}  
 
 
// main thread for the server application
int main(int argc, char *argv[])
{
    check=LISTEN;  // set state to LISTEN (first state)
    if(argc < 2){  // checks that there is a correct number of args. 
        fprintf(stderr, "ERROR, no port provided\n");
        exit(0);
   }
   
    port = atoi(argv[1]);
    port2= port+1;
    sock=socket(AF_INET, SOCK_DGRAM, 0);
    socket2=socket(AF_INET,SOCK_DGRAM,0);
    
    
    if (sock < 0|| socket2<0) error("Opening socket");
    length = sizeof(server);
    bzero(&server,length);
    bzero(&server2,length);
    server.sin_family=AF_INET;                             // setup first server addr
    server2.sin_family=AF_INET;                            // setup second server addr
    server.sin_addr.s_addr=INADDR_ANY;
    server2.sin_addr.s_addr=INADDR_ANY;
    server.sin_port=htons(port);
    server2.sin_port=htons(port2);
    if(bind(sock,(struct sockaddr *)&server,length)<0){    // bind first socket
        error("binding");
    }
    if(bind(socket2,(struct sockaddr *)&server2,length)<0){ // bind second socket
        error("binding");
    }
    fromlen = sizeof(struct sockaddr_in);
    if(pthread_mutex_init(&lock,NULL)!=0){
        error("Mutex uninitialized");
    }
    while(check!=CLOSED){ // loop until done with program
        switch(check){	
            case LISTEN:
                // wait for connection packet  -- Here client will send server packet with ID ='C' and a seq number
		// If anything other than 'C' - close socket as there is an error in this part of the protocol
                n=recvfrom(sock,buf,sizeof(buf),0,(struct sockaddr *) &from, &fromlen);
		if(n<0){ error("recvfrom: connection"); }
		n=recvfrom(socket2,buf,sizeof(buf),0,(struct sockaddr*)&from2,&fromlen);
		memcpy(&packID,buf,sizeof(packID));           // read ID off of buffer
	        off=sizeof(packID);                           // increase offset
		memcpy(&seq, buf+off, sizeof(seq));           // read seq number off of buffer
		temp=ntohl(seq);                              // set to host endianess
		if(temp==0 && packID=='C'){                   // connection request is correct 
                    write(1,"client wanting connection\n",27);
                    packID='A';                               // set ID to ack 
                    bzero(buf,sizeof(buf));
                    memcpy(buf,&packID,sizeof(packID));
                    memcpy(buf+off,&temp,sizeof(temp));       // changed seq to temp.
                    n=sendto(socket2,buf,sizeof(buf),0,(struct sockaddr*)&from2, fromlen);
                    if(n<0){ error("sendto:connection accepted"); } // send back to client showing connection accepted
                    check=REQUEST;
		}
		else{
                    close(sock);
                    close(socket2);
                }
                break;
            case REQUEST:
                // now waiting for file request from the client
                // Client packet should have ID of 'R' with seq number, file size, and the name of the file 
                n=recvfrom(sock,buf,sizeof(buf),0,(struct sockaddr *)&from,&fromlen);
		if(n<0){ error("recvfrom: file"); }           // read buffer
		   
		memcpy(&packID,buf,sizeof(packID));           // read packet ID
		off=sizeof(packID);
		memcpy(&seq,buf+off, sizeof(seq));            // read seq number
		temp=ntohl(seq);
		off=off+sizeof(seq); 
		memcpy(&sz,buf+off,sizeof(sz));               // read size
		off=off+sizeof(sz);
		tempSize=ntohl(sz); 
		memcpy(&fileAddr,buf+off,tempSize);           // read file name
		if(temp==1 && packID=='R'){                   // check to make sure the correct packet has been received
                    write(1,"client wanting file\n",20);
                    fileAddr[tempSize]='\0';                  // add terminating null
                    fp=fopen(fileAddr,"rb"); 
                    if(fp!=NULL){  // check if file exists 
                        fseek(fp, 0, SEEK_END);	              //seek to end of file
                        file_size = ftell(fp);	              // get the current file ptr
                        printf("server file size: %d\n", file_size);
                        fseek(fp, 0, SEEK_SET); 	      // reset back to the beginning of file
                        write(1,"file found for client\n",22);
                        packID='A';                           // now change packet ID to A to send back to client
                        bzero(buf,sizeof(buf));
                        memcpy(buf,&packID,sizeof(packID));
                        off=sizeof(packID);
                        memcpy(buf+off,&seq,sizeof(temp));
                        off+=sizeof(seq);
                        memcpy(buf+off,&file_size,sizeof(file_size));         
                        n=sendto(socket2,buf,sizeof(buf),0,(struct sockaddr*)&from2, fromlen);
		        if(n<0){ error("sendto:file found"); } // send the update to the client showing file exists
                    }
                    else{
	                write(1,"file not found\n",15);
                        packID='T';
                        bzero(buf,sizeof(buf));
                        memcpy(buf,&packID, sizeof(packID));
                        off=sizeof(packID);
                        memcpy(buf+off,&seq,sizeof(temp));
                        n=sendto(socket2,buf,sizeof(buf),0,(struct sockaddr*)&from2, fromlen);
                        if(n<0){ error("sendto:file found"); }
                        bzero(buf,sizeof(buf)); 
                        n=recvfrom(sock,buf,sizeof(buf),0,(struct sockaddr *) &from, &fromlen);
                        if(n<0){ error("closing after file failure"); }
                        memcpy(&packID,buf,sizeof(packID));
                        off=sizeof(packID);
                        memcpy(&seq,buf+off,sizeof(seq));
                        temp=ntohl(seq);
                        if(packID =='A'){ 
                            close(sock);
                            close(socket2);
                            write(1,"closing server sock\n",20);
                        } 
                   }
	       } 

               bzero(buf, sizeof(buf));
               n=recvfrom(sock, buf, sizeof(buf),0,(struct sockaddr *) &from, &fromlen);
               if(n<0){ error("Closing after not receivig ACK"); }
               memcpy(&packID,buf,sizeof(packID));
               off=sizeof(packID);
               memcpy(&seq,buf+off,sizeof(seq));
               temp=ntohl(seq);
               check=SENDFILE;
               break;
		  	   
           case SENDFILE: // state for sending the file from server to client.
               for(i=0;i<50;i++){ 
                   // initialize array
                   acked[i]=0; 
               }
               fileNotDone=0; // set the control variables
               fileEndReached=0;
               lowestAck=0;
               windowSize=5;
               seq=0;
               lastAck=0;	
			
               threadValue = pthread_create(&ackThread, NULL, ackFunction, (void*)(intptr_t) sock); // create ackthread
               if((threadValue)<0){ error("issue creating ack thread"); }

               if(packID =='A'){  
                  while(!fileNotDone){
                      // maybe not the most efficient - checks the time before each send -
                      // does before() need to be initialized 
                      difference=clock()-before;
                      msec=difference*1000 /CLOCKS_PER_SEC;
                      if(msec>0){ // if time expired - set the current seq to the lowestAck
                          pthread_mutex_lock(&lock);
                          seq=lowestAck;
                          pthread_mutex_unlock(&lock);
                      }
					
                      if((seq-lowestAck)<windowSize && !fileEndReached){ // loop while still in window size
                          pthread_mutex_lock(&lock);                     // grab lock
                          bytesRead = fread((buf+sizeof(char) + sizeof(int)*2), 1, 1024, fp);
                          if(bytesRead<1024){
                              fileEndReached=1;                          // if reading less than max buffer - end of file 
                              lastAck=seq;
                          }
                          if(lowestAck==seq){
                              before=clock();                            // if current lowestAck being sent- initialize the before() time count
                          }
					   
                          packID = 'D';                                  // Data PackID
					   
                          // To be compared with receiving seq
                          memcpy(buf, &packID, sizeof(packID));    
                          off = sizeof(packID);               
                          temp=htonl(seq);
                          memcpy(buf+off, &temp, sizeof(seq));
                          off+=sizeof(tempSize);
                          sz = bytesRead;
                          tempSize=htonl(sz);
                          memcpy(buf+off,&tempSize,sizeof(tempSize));
                          if(acked[seq]!=1){
                              n = sendto(socket2,buf,sizeof(buf),0,(struct sockaddr*)&from2, fromlen);
                              if(n<0){ error("client: packet sending failure"); }
                          }
                          seq++; // increase seq
					   
						
                      }
                      else{
                          sleep(1); // adding sleep so we dont loop infinitely- could change to usec() to shorten time interval
                      }
                      pthread_mutex_unlock(&lock);
  
                  }		  
              }
          pthread_join(ackThread,NULL); // join thread back to original
          check=DISCONNECT;             // start disconnect process
          break;

          case DISCONNECT: // state to send T disconnect packet
              packID='T';
              bzero(buf,sizeof(buf));
              memcpy(buf,&packID, sizeof(packID));
              off=sizeof(packID);
              memcpy(buf+off,&seq,sizeof(temp));
              n=sendto(socket2,buf,sizeof(buf),0,(struct sockaddr*)&from2, fromlen);
              if(sock){ close(sock); }
              if(socket2){  close(socket2); }
              check=CLOSED; // now exit the while loop at top
              break;
        }
    }
    return 0;
}
